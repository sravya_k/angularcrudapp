import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatDatepicker } from '@angular/material/datepicker';
import { FormGroup, FormControl } from '@angular/forms';
import { DataService, User } from '../data.service';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Inject } from '@angular/core';

@Component({
  selector: 'app-create-insurance',
  templateUrl: './create-insurance.component.html',
  styleUrls: ['./create-insurance.component.scss']
})
export class CreateInsuranceComponent implements OnInit {

  insuranceForm = new FormGroup({
    policyNumber: new FormControl(''),
    policyStartDate: new FormControl(new Date()),
    policyEndDate: new FormControl(new Date()),
    policyDescriptions: new FormControl(''),
    customerFirstName: new FormControl(''),
    customerSurname: new FormControl(''),
    customerDateOfBirth: new FormControl(''),
  });
  formHeaderType = "Create Insurance";

  constructor(private Dialog: MatDialog, public data: DataService, @Inject(MAT_DIALOG_DATA) public dialogData: any, private dialogRef: MatDialogRef<CreateInsuranceComponent>) {

  }

  ngOnInit(): void {
    if(this.dialogData.type == 'edit') {
      this.formHeaderType = "Edit Insurance";
      this.insuranceForm.setValue({
        policyNumber: this.dialogData.data.policynumber,
        policyStartDate: this.dialogData.data.policystartdate,
        policyEndDate: this.dialogData.data.policyenddate,
        policyDescriptions: this.dialogData.data.policydescriptions,
        customerFirstName: this.dialogData.data.customerfirstname,
        customerSurname: this.dialogData.data.customersurname,
        customerDateOfBirth: this.dialogData.data.customerdateofbirth,
      });
    }
  }

  onSubmit() {
    if(this.dialogData.type == 'edit') {
      this.editUser();
    }
    else {
      this.addUser();
    }
  }

  addUser() {
    this.data.addUser(this.insuranceForm.value).subscribe((resp: any) => {
      this.dialogRef.close('closing');
    })
  }

  editUser() {
    this.data.updateUser(this.dialogData.data.policynumber, this.insuranceForm.value).subscribe((resp: any) => {
      this.dialogRef.close('closing');
    })
  }

}
