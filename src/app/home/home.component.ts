import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { CreateInsuranceComponent } from '../create-insurance/create-insurance.component';
import { DataService } from '../data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  users: any = [];

  fileNameDialogRef?: MatDialogRef<CreateInsuranceComponent>;

  dataSource = this.users;
  displayedColumns: string[] = ['policynumber', 'policystartdate', 'policyenddate', 'policydescriptions', 'customerfirstname', 'customersurname', 'customerdateofbirth', 'actionsColumn'];

  constructor(public dialog: MatDialog, public data: DataService) { 
    this.getUsers();
  }

  ngOnInit(): void {
    
  }

  getUsers(): void {
    this.data.getUsers().subscribe(resp => {
      this.users = resp;
      this.dataSource = resp;
    });
  }

  createInsurance() {
    this.fileNameDialogRef = this.dialog.open(CreateInsuranceComponent, {
        data: {
            type: "create"
        }
    });
  
    this.fileNameDialogRef.afterClosed().subscribe(result => {
      this.getUsers();
    });
  }

  deleteInsurance(policyNumber:string) {
    this.data.deleteUser(policyNumber).subscribe(resp => {
      this.getUsers();
    });
  }

  editInsurance(insuranceDetails: any) {
    this.fileNameDialogRef = this.dialog.open(CreateInsuranceComponent, {
      data: {
          type: "edit",
          data: insuranceDetails
      }
    });
    this.fileNameDialogRef.afterClosed().subscribe(result => {
      this.getUsers();
    });
  }

}
