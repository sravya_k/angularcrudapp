import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/internal/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map } from 'rxjs/operators';

export interface User {
  policyNumber: string,
  policyStartDate: Date,
  policyEndDate: Date,
  policyDescriptions: string,
  customerFirstName: string,
  customerSurname: string,
  customerDateOfBirth: Date,
}

const endpoint = 'http://localhost:3000/';
@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { 

  }

  // function to get all the users
  getUsers(): Observable<any> {
    return this.http.get<User[]>(endpoint + 'users').pipe(
      catchError(this.handleError)
    );
  }

  // function to create a user
  addUser(user: any): Observable<any> {
    return this.http.post(endpoint + 'users', user).pipe(
      catchError(this.handleError)
    );
  }

  // delete
  deleteUser(policyNumber: string): Observable<any> {
    return this.http.delete(endpoint + 'users/' + policyNumber).pipe(
      catchError(this.handleError)
    );
  }

  // update
  updateUser(policyNumber: string, user: User): Observable<any> {
    return this.http.put<User>(endpoint + 'users/' + policyNumber, user).pipe(
      catchError(this.handleError)
    );
  }

  

  private handleError(error: HttpErrorResponse): any {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError(
      'Something bad happened; please try again later.');
  }
}
